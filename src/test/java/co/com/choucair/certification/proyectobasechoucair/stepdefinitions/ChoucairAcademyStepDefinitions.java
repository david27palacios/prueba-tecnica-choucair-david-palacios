package co.com.choucair.certification.proyectobasechoucair.stepdefinitions;

import co.com.choucair.certification.proyectobasechoucair.model.AcademyChoucairData;
import co.com.choucair.certification.proyectobasechoucair.questions.Answer;
import co.com.choucair.certification.proyectobasechoucair.tasks.Login;
import co.com.choucair.certification.proyectobasechoucair.tasks.OpenUp;
import co.com.choucair.certification.proyectobasechoucair.tasks.Search;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;

import java.util.List;


public class ChoucairAcademyStepDefinitions {


    @Before
    public void setStage(){
        OnStage.setTheStage(new OnlineCast());
    }

    @Given("^: than Rose want to learn automation at the academy Choucair$")
    public void thanRoseWantToLearnAutomationAtTheAcademyChoucair(List<AcademyChoucairData> academyChoucairData) throws Exception{
        OnStage.theActorCalled("Rose").wasAbleTo(OpenUp.thePage(),Login.OnThePage(academyChoucairData.get(0).getStrUser(),academyChoucairData.get(0).getStrPassword()));
    }

    @When("^: she search for the course on the Choucair Academy platform$")
    public void sheSearchForTheCourseISTQBTestAutomationEngineerOnTheChoucairAcademyPlatform(List<AcademyChoucairData> academyChoucairData) throws Exception {
        OnStage.theActorInTheSpotlight().attemptsTo(Search.the(academyChoucairData.get(0).getStrCourse()));
    }

    @Then("^: she finds course called$")
    public void sheFindsCourseCalledISTQBTestAutomationEngineer(List<AcademyChoucairData> academyChoucairData)throws Exception {
        OnStage.theActorInTheSpotlight().should(GivenWhenThen.seeThat(Answer.toThe(academyChoucairData.get(0).getStrCourse())));
    }
}