package co.com.choucair.certification.proyectobasechoucair.userinterface;


import co.com.choucair.certification.proyectobasechoucair.tasks.Search;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.WhenPageOpens;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;



public class SearchCoursePage extends PageObject{

    public static final Target BUTTON_COURSE = Target.the("button that shows the courses actives").located(By.xpath("//*[@id=\"certificaciones\"]/div[1]/a"));
    public static final Target INPUT_COURSE = Target.the("input where we fill in the name of the course").located(By.xpath("//*[@id=\"coursesearchbox\"]"));
    public static final Target BUTTON_SEARCH = Target.the("button that showing the course searched").located(By.xpath("//*[@id=\"coursesearch\"]/fieldset/button"));
    public static final Target NAME_COURSE = Target.the("Name course searched").located(By.xpath("//*[@id=\"region-main\"]/div/div[1]/div/div[1]/h3/a"));
    public static final Target BUTTON_SELECTOR = Target.the("Name course searched").located(By.partialLinkText("ISTQB - Test Automation Engineer"));

}





